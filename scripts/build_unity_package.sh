#!/bin/sh
# Execute this script in the top project folder.

UNITY=/Applications/Unity/Hub/Editor/2020.3.38f1/Unity.app/Contents/MacOS/Unity

$UNITY -gvh_disable -batchmode \
  -projectPath SampleProject-2019.4 \
  -exportPackage Assets/Usercentrics Assets/ExternalDependencyManager Usercentrics.unitypackage \
  -quit
  
